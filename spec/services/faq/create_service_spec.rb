require_relative '../../spec_helper'

describe FaqModule::CreateService do
  before do
    @question = FFaker::Lorem.sentence
    @answer = FFaker::Lorem.sentence
    @hashtags = "#{FFaker::Lorem.word}, #{FFaker::Lorem.word}"
  end

  describe '#call' do
    context "without hashtags params" do
      it 'will receive a error' do
        @createService = FaqModule::CreateService.new({ 'question' => @question, 'answer' => @answer })
        response = @createService.call()
        expect(response).to match("Hashtag Obrigatória")
      end
    end

    context 'With valid params' do
      before do
        @createService = FaqModule::CreateService.new({ 'question' => @question, 'answer' => @answer, 'hashtags' => @hashtags })
        @response = @createService.call()
      end

      it 'receive success message' do
        expect(@response).to match("Criado com sucesso")
      end

      it 'question and answer is present in database' do
        faq = Faq.last
        expect(faq.question).to match(@question)
        expect(faq.answer).to match(@answer)
      end

      it 'hashtags are created' do
        hashtags = @hashtags.split(/[\s,]+/)
        expect(hashtags.first).to match(Hashtag.first.name)
        expect(hashtags.last).to match(Hashtag.last.name)
      end
    end
  end
end
